# DO'S AND DON'TS
![](assignments/images/11.jfif)
## 1. POWER FACTOR
* All the digtal device specially IOt device consumed small power 
* Please make sure to provide the required amout of power to devices otherwise it will get damages
* Make sure the periferral device that consume more power (greter than 12v ) provide power sepratelly.
* > ex = devices like geared motors consumes more power when work with load if connencted durectly with the microcontroller supply
the microcontroller board get

## Do's
> Always grip the connection port while coonecting any usb the hardware is not good in term of strength
> Always make sure that the output voltage of the power supply matches the input voltage of the board.
> Always check the max power consumption (normal mode and active mode)


## Don'ts
> Do do not connect or remove any device while power on 
> Do not connect power supply without matching the power rating.
> Never connect a higher output(12V/3Amp) to a lower (5V/2Amp) input 

## 2. Handling
![link](assignments/images/ece.jfif)
Do’s:

* Developmet board or microcontroller borad are not very durable in term of strength so handel with .
* While working keep the board on a flat stable surface (wooden table) .
* Unplug the device before performing any operation on them.
* When handling electrical equipment, make sure your hands are dry.
* Keep all electrical circuit contact points enclosed.
* Try to avoid max power consuption if borad get heated try proper fan installation.

Don’ts

* Don’t handle the board when its powered ON.
* Never touch electrical equipment when any part of your body is wet, (that includes fair amounts of perspiration).
* Usually capacitor contain power even after some times of power off make sure to avoid touching pin directly .

## 3. GPIO
Do’s:

* Always connect to required power rating (3v ,5v)
* Always connect resistor while connecting leds or device less than (3v)
* To Use 5V peripherals with 3.3V we require a logic level converter.

Don’ts

* Never connect anything greater that 5v to a 3.3v pin.
* void making connections when the board is running.
* Don't plug anything with a high (or negative) voltage.
* Do not connect a motor directly , use a transistor to drive it .
## Guidelines for using interfaces(UART,I2C,SPI)

# 1.UART
![](assignments/images/uart.png)
>(Universal asynchronous reciever and transmitter )
* UART is  basic mode of communication for one microcontroller to another or microcontroller to computer
* Asynchronous serial communication
* communicate with board through USB to TTL connection.It doesn't require any protection circuit.
* Only required two wire for communication and asynchronous serial communication
* Connect Rx device1 to Tx device2 and  Tx device1 to Rx device2.
* If the device1 works on 5v and device2 works at 3.3v then use the level shifting mechanism(voltage divider).
* But Senor interfacing using UART require a protection circuit.


# 2.I2C
![](assignments/images/i2c.png)
> (IIC or Inter-Integrated Circuit) 
* 
* using I2C interfaces with sensors SDA and SDL lines must be protected.
* Protection of these lines is done by using pullup registers on both lines.
* If you use the inbuilt pullup registers it doesn't require an external circuit.
* If you are using bread-board to connect your sensor,use the pullup resistor(2.2kohm <= 4K ohm).
![](assignments/images/Screenshot__57_.png)


# 3.SPI
![](assignments/images/spi.png)
> (Serial Peripheral Interface)
* SPI in development boards does not require any protection circuit.
* when you are using more than one slaves device2 can "hear" and "respond" to the master's communication with device1 which is an disturbance.
* To overcome the disturbance,we use a protection circuit with pullup resistors(1kOhm ~10kOhm,Gener*ally 4.7kOhm) on each the Slave Select line(CS).
![](assignments/images/Lesson_3__UART_Operation___Topic_3__Universal_Asynchronous_Receiver-Transmitter__UART____mechatronics_Courseware___edX_-_Mozilla_Firefox_7_4_2020_7_03_57_AM__2_.png)