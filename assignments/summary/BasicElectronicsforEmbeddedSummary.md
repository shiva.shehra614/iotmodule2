# 1.SENSOR AND ACTUATAORS 
> Sensors or a transducer are devices that converts some physical energy into electrical signals that can be used to take a reading.
![](assignments/images/Electronics_kit_and_smart_phone_sensors___2.1_Sensors_and_actuators___IOT2x_Courseware___edX_-_Mozilla_Firefox_7_9_2020_1_47_26_AM__2_.png)
 ![](assignments/images/m1.png)
 ## Sensors behave as a detector of a system and provide information to system when some kind of change occurs specific type of sensor is use for specific change (movement).
  ### EXAMPLES
  * PID SENSOR
  * PIR SENSOR
  * ULTRASONIC
  * INFRARED 
  * ACCLEROMETER
  * FLEX SENSOR
  * GAS DETECTOR SENSOR
  * Water level  sensor
  * Temperature  sensor
  * Acoustic and noise IOT sensor
  * LDR sensor
  * Moisture sensor
## Actuators operates in the reverse direction of the sensor.
![](assignments/images/m2.png)
   > Actuators takes electrical input and converts it into a physical action.
  ![link](assignments/images/index.jfif)  
  ## EXAMPLES
  **Electric motor, hydraulic system and a pneumatic sensor.**
  * Linear actuators 
  * Motors
  * Relays 
  * Solenoids


  ## 2.Analog and Digital Signals
  > Signal is a physical quantity that carries information and is dependent on independent parameters.
  ### Analog Vs Digital
![](assignments/images/analog_vs_digital.png)
**Analog Signals**

- It is a continous signal where both amplitude and time is continuous.
-Analog signal represent a value for every bit of 
- These are denoted by sinusodial waves. And carries information using electric pulses.
>  Examples:All the enviornment singnal,Human voice in air, analog electronic devices.
- Analog technology records waveforms as they are.
> Applications:Communication sector

**Digital Signals**
![](assignments/images/avd.png)
- It is a discrete signal where amplitude is continuous and time is discrete.
- These are denoted by square waves. And carries information using binary digits.
> Examples:Computers, CDs, DVDs, and other digital electronic devices.
- digital signal is the result of analog signal where the values are taken at only discrete points.
> Applications:FIBER COMMUNICATION,DIGITAL COMMUNICATION

## 3.Micro-controllers Vs Micro-processors
![link](assignments/images/uc1.jfif)

**Micro-processors**

- Micro-processor is a program controlled device that functions as A (BRAIN) cpu of the computer, which read instructions from memory,fetches and execute the instructions.
-it only proceess the instruction,
- micropcessor needs external peripherals for performing tasks.
> Examples: 8085,8086,80186,80286 ..., Micro-processors.
- It doesn't have internal memory like:RAM,ROM.

**Micro-controllers**

- Micro-controller is a chip which includes Micro-processors,memory,I/O ports all in one single chip.
> Example:ARM CORTEX MSP432,ATMEGA.
- Microcontroller is itself a complete computer
- It has internal memory such as RAM,ROM and other peripherals.
- It does not need any external circuits to do its tasks.
## 4.Introduction to RPi

![](assignments/images/rpi.png)

- Raspberry Pi is like a mini computer.
- It has hdmi port erathernet port i\p /o\p port .
- we can upload os on it it can support linux os.
- It provides GPIO(general purpose input/output) pins and also posses large power for its size.
- We Can connect multiple Pi’s together. It can also be connected to sensors and actuators to collect data and perform operations.
- It is a SOC (System On Chip).We can connect shields (Shields - addon functionalities).
- Pi uses ARM(Acorn RISC Machine).
- **Raspberry Pi Interfaces:** GPIO,UART,SPI,I2C,PWM

## 5.Serial and Parallel Communication

![](assignments/images/serial.png)
![](assignments/images/parallel-communication.jpg)

- In parallel communication all the binary digits are sent at same time for processing.
***Parallel Interfaces:GPIO***
- Where as in serial communication binary digits are sent one after other.
***Serial Interfaces:UART,SPI,I2C***
> maybe parallel seems better option but we use serail communication because it has far advantage over parallel communication.