# IIoT Protocols

> Protocols are the set of rule through which devices communicate with each others.(standard way of commanding some instruction)

## IIoT(Industrial Internet of Things) Protocols

##  4-20mA

- The 4-20 mA current loop is favourable and major control signal in many industries.
- 0 amp is not possible with power drawing circuit so  we choose 4-20 mA
- Information through analog signals is transmitted via varying amounts of voltage or current.
- The range of current is taken between 4-20mA because:
    - 0--20 signal does not diferentiate between off and error state
    - cost effective (cheaper than the pressure system)
    - Signals below 4mA would be unrecognizable.
    - Easier to differentiate a live zero (4mA) signal from a failure in the system (0mA).
-  current is easier to handel then pressure system and industries start prefering it.

![](assignments/images/4-20.png)
- Current is same in all places throughout the loop.It remains constant.
- 0 amp is not possible with power drawing circuit so  we choose 4-20 mA
- Here Vtot is the power supply and (R1,R2,R3) are resistive loads.
- Due to the Voltage the current is driven and flows through the loop constantly, but there will certain voltage drop at loads(calculate by Ohm's law(V=IR)).
- The reason for choosing 4-20mA loop is, 


## Modbus
![](assignments/images/Modbus-Plant.png)
- Modbus is a communication protocol for use with its programmable logic controllers (PLC).
- Modbus supports communication to and from multiple devices connected to the same cable or Ethernet network.
- The device that requests information is Master and which supplies information is **Slave**.Information is transfered through ### Master-Slave communication.
- Function code is a frame where the communication between Master and Slave takes place.
- slave takes instructions and responds, based on the function code received.
- Protocols are used as a local interface to manage devices.Modbus can be used over 2 interfaces:
    - RS485 - called as Modbus RTU
    - Ethernet - called as Modbus TCP/IP

## RS485
- RS485 is a serial communication method allows multiple devices (up to 32) to communicate.
- RS485 is not directly compatible,its easy to use an RS485 to USB.



### OPCUA

![](assignments/images/opc_ua.png)
- OPCUA: Open Platform Communications United Architecture.
- OPCUA allows you to communicate with industrial hardware devices(machine to machine) for automation.
- OPCUA is implemented in server/client pairs.
- Client acts as master it decides what to do where Server waits for clients request and reponds accordingly.
- OPCUA protocol is used widely due do its efficient performace,high scalibility and security.
- OPCUA  has protocols that are self reliant and idepentent.They are:Data access(DA),Alarm & Events(AE),Historical Analysis(HDA).
-  It is not designed for one platform it can work on any platform.

**Client-Server**

![](assignments/images/opcua.png)

## Cloud Protocols

### MQTT
![](assignments/images/mqtt.png)
- MQTT:Message Queuing Telemetry Transport.
- MQTT is publish-subscribe network protocol that helps us transfer messages between devices.
- It is bandwidth-efficient,bidirectional and uses less battery power.
- The MQTT protocol allows your SCADA system to access IIoT data.
- MQTT broker plays a role of sending/transfering messages to the devices.
- Each device(MQTT client) can subscribe to particular topics(Each slash indicates a topic level).
-  When another client publishes a message on a subscribed topic, the broker forwards the message to any client that has subscribed.

![](extras/m10.png)

### HTTP
![](assignments/images/http.png)
- HTTP:Hyper Text Transfer Protocol.
- It was designed for communication between web browsers and web servers.
- HTTP is a protocol which allows the fetching of resources, such as HTML documents.
- It is a client-server protocol.client sends an HTTP request and Server sends HTTP response.
**Request**
> Request has three parts:
- Request line
- HTTP headrs
- message body
> Requesting Methods:
- GET request is a type of HTTP request using the GET method.
![](assignments/images/mqtt1.png)

> GET / HTTP/1.1

> Host: xkcd.com

> Accept: text/html
  

- GET: Retrieve the resource from the server (e.g. when visiting a page).
- POST: Create a resource on the server (e.g. when submitting a form).
- PUT/PATCH:Update the resource on the server (used by APIs).
- DELETE:Delete the resource from the server (used by APIs).

**Response**
> Response has three parts:
* Status line
* HTTP header
* Message body









